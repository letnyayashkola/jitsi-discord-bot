import logging
import os

import aiohttp
from discord.ext import commands

try:
    from dotenv import load_dotenv
except:
    pass
else:
    load_dotenv()

logging.basicConfig(
    format="%(asctime)s %(message)s", datefmt="%Y-%m-%d %H:%M:%S", level=logging.INFO
)

bot = commands.Bot(command_prefix="/")


@bot.event
async def on_ready():
    logging.info("Logged in as {0.user}".format(bot))


rooms_url = "https://moderated.jitsi.net/rest/rooms/"

message = "Линк для смертных\n{joinUrl}\n\nЛинк для богов\n{moderatorUrl}"


@bot.command()
async def jitsi(ctx):
    async with aiohttp.ClientSession() as session:
        async with session.get(rooms_url) as resp:
            room = await resp.json()

        async with session.get(rooms_url + room["meetingId"]) as resp:
            meeting = await resp.json()

    logging.info(meeting)

    await ctx.send(message.format(**meeting))


if __name__ == "__main__":
    bot.run(os.getenv("BOT_TOKEN"))
