FROM python:3.9-alpine as builder

ENV PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    PIP_NO_CACHE_DIR=on \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100

RUN apk update \
    && apk add --no-cache \
    gcc \
    musl-dev \
    libffi-dev \
    openssl-dev \
    # rust for build cryptography package
    cargo \
    && pip install poetry

WORKDIR /app

COPY poetry.lock .
COPY pyproject.toml .

RUN poetry config virtualenvs.in-project true \
    && poetry install --no-root --no-dev --no-interaction

FROM python:3.9-alpine

WORKDIR /app

COPY --from=builder /app/.venv /app/.venv
COPY . .

ENV PATH="/app/.venv/bin:$PATH"

CMD [ "python", "main.py" ]
